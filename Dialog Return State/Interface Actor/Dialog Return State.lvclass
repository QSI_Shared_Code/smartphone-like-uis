﻿<?xml version='1.0' encoding='UTF-8'?>
<LVClass LVVersion="20008000">
	<Property Name="NI.Lib.ContainingLib" Type="Str">Dialog Return State.lvlib</Property>
	<Property Name="NI.Lib.ContainingLibPath" Type="Str">../../Dialog Return State.lvlib</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;7R=2MR%!81N=?"5X&lt;A91M&lt;/W-,&lt;'&amp;&lt;9+K1,7Q,&lt;)%N&lt;!NMA3X)DW?-RJ(JQ"I\%%Z,(@`BA#==ZB3RN;]28_,V7@P_W`:R`&gt;HV*SU_WE@\N_XF[3:^^TX\+2YP)D7K6;G-RV3P)R`ZS%=_]J'XP/5N&lt;XH,7V\SEJ?]Z#5P?=J4HP+5JTTFWS%0?=B$DD1G(R/.1==!IT.+D)`B':\B'2Z@9XC':XC':XBUC?%:HO%:HO&amp;R7QT0]!T0]!S0I4&lt;*&lt;)?=:XA-(]X40-X40-VDSGC?"GC4N9(&lt;)"D2,L;4ZGG?ZH%;T&gt;-]T&gt;-]T?.S.%`T.%`T.)^&lt;NF8J4@-YZ$S'C?)JHO)JHO)R&gt;"20]220]230[;*YCK=ASI2F=)1I.Z5/Z5PR&amp;)^@54T&amp;5TT&amp;5TQO&lt;5_INJ6Z;"[(H#&gt;ZEC&gt;ZEC&gt;Z$"(*ETT*ETT*9^B)HO2*HO2*(F.&amp;]C20]C2)GN4UE1:,.[:/+5A?0^NOS?UJ^3&lt;*\9B9GT@7JISVW7*NIFC&lt;)^:$D`5Q9TWE7)M@;V&amp;D,6;M29DVR]6#R],%GC47T9_/=@&gt;Z5V&gt;V57&gt;V5E&gt;V5(OV?^T[FTP?\`?YX7ZRP6\D=LH%_8S/U_E5R_-R$I&gt;$\0@\W/VW&lt;[_"&lt;Y[X&amp;],0^^+,]T_J&gt;`J@_B_]'_.T`$KO.@I"O[^NF!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">536903680</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LVClass.ClassNameVisibleInProbe" Type="Bool">true</Property>
	<Property Name="NI.LVClass.IsInterface" Type="Bool">true</Property>
	<Property Name="NI.LVClass.LowestCompatibleVersion" Type="Str">1.0.0.0</Property>
	<Item Name="Dialog Dismiss Action Enum.ctl" Type="VI" URL="../Dialog Dismiss Action Enum.ctl">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!$+!!!!!1$#!0%!!!!!!!!!!RF%;7&amp;M&lt;W=A5G6U&gt;8*O)&amp;.U982F,GRW&lt;'FC'U2J97RP:S"3:82V=GYA5X2B&gt;'5O&lt;(:D&lt;'&amp;T=RZ%;7&amp;M&lt;W=A2'FT&lt;7FT=S""9X2J&lt;WYA27ZV&lt;3ZD&gt;'Q!:5!7!!136WFO:'^X=S!C7#)A1H6U&gt;'^O#U^L98EA1H6U&gt;'^O$5.B&lt;G.F&lt;#"#&gt;82U&lt;WY71WRJ9WNF:#"0&gt;82T;72F)%2J97RP:Q!62'FB&lt;'^H)%2J=WVJ=X-A17.U;7^O!!%!!!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">1048576</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1082143232</Property>
	</Item>
	<Item Name="Dialog Return State.vi" Type="VI" URL="../../Dialog Return State.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)F!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;B!=!!?!!!X'52J97RP:S"3:82V=GYA5X2B&gt;'5O&lt;(:M;7)&lt;2'FB&lt;'^H)&amp;*F&gt;(6S&lt;C"4&gt;'&amp;U:3ZM&gt;G.M98.T!"&gt;%;7&amp;M&lt;W=A5G6U&gt;8*O)&amp;.U982F)'^V&gt;!!71&amp;!!!Q!!!!%!!ABF=H*P=C"J&lt;A!!QA$R!!!!!!!!!!-:2'FB&lt;'^H)&amp;*F&gt;(6S&lt;C"4&gt;'&amp;U:3ZM&gt;GRJ9BN%;7&amp;M&lt;W=A5G6U&gt;8*O)&amp;.U982F,GRW9WRB=X-?2'FB&lt;'^H)%2J=WVJ=X-A17.U;7^O)%6O&gt;7UO9X2M!'6!&amp;A!%%F&gt;J&lt;G2P&gt;X-A)FAC)%*V&gt;(2P&lt;AN0;W&amp;Z)%*V&gt;(2P&lt;AV$97ZD:7QA1H6U&gt;'^O&amp;E.M;7.L:71A4X6U=WFE:3"%;7&amp;M&lt;W=!&amp;52J97RP:S"%;8.N;8.T)%&amp;D&gt;'FP&lt;A"91(!!(A!!.RF%;7&amp;M&lt;W=A5G6U&gt;8*O)&amp;.U982F,GRW&lt;'FC'U2J97RP:S"3:82V=GYA5X2B&gt;'5O&lt;(:D&lt;'&amp;T=Q!72'FB&lt;'^H)&amp;*F&gt;(6S&lt;C"4&gt;'&amp;U:3"J&lt;A!!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!(!!A#!!"Y!!!.#!!!!!!!!!!!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!%!!!!*!!!!!!!1!*!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1342710272</Property>
	</Item>
</LVClass>
