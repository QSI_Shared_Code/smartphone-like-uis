﻿<?xml version='1.0' encoding='UTF-8'?>
<LVClass LVVersion="20008000">
	<Property Name="NI.Lib.ContainingLib" Type="Str">Swipe Screen.lvlib</Property>
	<Property Name="NI.Lib.ContainingLibPath" Type="Str">../../Swipe Screen.lvlib</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*+!!!*Q(C=\&gt;7R=&gt;N!%)8B:Y]$2:JBA.T$S0GWQ":1AL=&amp;JAI=M!%''SB1I)16?)9N\+A$NM!7[0]/+RB/2!83S)%!,UW]!`9_!D?1V,=&lt;[:MOV\&lt;BR?UVY[V*/[V^8B&lt;"&gt;.S(JO`$(!_,];G?^\`"0$Y@$(/TZ@DU'`Y:8X2^A^`X]PC64:`&lt;Z`;RW^X62@LO;`;,WIO)3CIIJURNKEO3*XG3*XG3*XG1"XG1"XG1"\G4/\G4/\G4/\G2'\G2'\G2'XH@S55O=J&amp;$3C:0*EK;*AW3E[%I_5A]C3@R*"Y/F8A34_**0)G(5Z2Y%E`C34S*B]O5?"*0YEE]C9&gt;78:*^*]?4?'CPQ".Y!E`A#4R-K=!4!),*AM:"%RA+"I-PA3@Q""[_+P!%HM!4?!)0QQI]A3@Q"*\!QS8^LE48N*U=$WXE?"S0YX%]DI@7=DS/R`%Y(M@$&gt;()]DM&gt;"/"-[T3()O=AZQ4FQ0)[(`_2Y()`D=4S/B[(_B,T@G;:J/TE?QW.Y$)`B-4SUE/%R0)&lt;(]"A?WMLQ'"\$9XA-$V0*]"A?QW.!D%G:8E9TYU,D*#-Q00TLK]8[5YIOM&lt;Z,N8B6CV+VW&amp;3,3,5Y6!^&gt;^4"6$UFV]V5X687T6$&gt;"^?.5;"6'.9HKYH;CTHS?K#.VI(&lt;5FBKJ$&lt;7G6OX3.T\R@$\L&gt;$LJ?$TK=$BIN^NJO^VK(%&gt;N.BONVWON6KPZ.@#&gt;@8YB4/_F/\[XOB_?^A`DDXW-O@`V?,0``8D\]XGMV4P^,@U0XIX[KG6@\N%@Y^&gt;_C!!!!!!</Property>
	<Property Name="NI.Lib.LocalName" Type="Str">Swipe Screen UI</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">536903680</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.2</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Property Name="NI.LVClass.ClassNameVisibleInProbe" Type="Bool">true</Property>
	<Property Name="NI.LVClass.DataValRefToSelfLimitedLibFlag" Type="Bool">true</Property>
	<Property Name="NI.LVClass.FlattenedPrivateDataCTL" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!$015F.31QU+!!.-6E.$4%*76Q!!,T1!!!3=!!!!)!!!,R1!!!!P!!!!!B*4&gt;WFQ:3"49X*F:7YO&lt;(:M;7)85X&gt;J='5A5W.S:76O)&amp;6*,GRW9WRB=X-!!!!!I#!!A!!!-!!!+!!!!!!!!!1!!Q!]!,Q!(U#!!A!!!!!"!!%!"P````]!!!!!!!!!!!!!!!$#O`M;,&lt;Z]1\;ZR@I/#K!B!!!!$!!!!"!!!!!!N+O+NE$&amp;HEOD[&amp;]1DB&lt;S=&gt;1&gt;D.G0!,)%[9!*G/TY1HY!!!!!!!!!!+;:2[6"?"2,BHO874D_E?A"!!!!`````^1&gt;D.G0!,)%[9!*G/TY1HY!!!!1,Z9-&gt;-TYSCU6YFL!&gt;J+;&amp;!!!!!1!!!!!!!!!*Q!"4&amp;:$1Q!!!!%!!F:*4%)!!!!!5&amp;2)-!!!!!5!!1!"!!!!!!)!!Q!!!!!#!!%!!!!!!#5!!!!I?*RDY'*A;G#YQ!$%D%$-V-$U!]D_!/)T#(!A3!$&lt;Z1M+!!!!!!!!3Q!!!2BYH'.AQ!4`A1")-4)Q-.U!UCRIYG!;RK9GQ'5O,LOAYMR1.\*#2"G"9ER\A!QGE"R5$1@9/QR-&amp;Y$Y",IZ,/A#1!!!C6MI.!!!!!!-!!&amp;73524!!!!!!!$!!!"@!!!!NRYH'NA:'$).,9Q/Q#EG9&amp;9A;'")4E`*:7,!=BHA)!D4!Q5AQ#I?6JIYI9($K="A2[`@!O9X_WCQN*=I],$6-LXPU3&amp;)_!&amp;3,$Z#-@B&lt;I_=YYYW9#5=71R:$!(`!T/;D`#!&gt;30L^V&amp;B-4T15+H-5#J]P.'%%7*,).19FM.B1!5C1%M%OE.5/$K&lt;6)"O[OT3!*,&gt;BJ9'J@+^"3S&gt;GU##P96!"FA]!'C?&gt;3!,`Z1$=9?""0]W))XK8K#2(!=@MP2/"!NUAMD/%%;*YS[-/G*!&gt;C]09S$#(WDO$?M'_D+,-1QGX]VWX%%$R$\O)!+B-C"5"91K!&amp;%\1%4=92#Z^P7^8;"Q:5-+5Q=I&lt;A"C5,T#M"Y$)Q-I1*C!%'4KH````^M!2:CA9IJ1-2$\*:3NA;3(ER%CZI"E$UAPS)3T1&amp;I$SLY-:4&gt;!X153%Q8K,9#S:2AB[1(%6A;Z$=L7!L)&amp;I'R$2D!$T$;$MA^!9R/&gt;&gt;P:X=56/5\$U#A#5U8MR!!!!$C!"A!!!!!9S-#YQ,D%!!!!!!!!-)!#!!!!!"$)Q,D!!!!!!$C!"A!!!!!9S-#YQ,D%!!!!!!!!-)!#!!!!!"$)Q,D!!!!!!$C!"A!!!!!9S-#YQ,D%!!!!!!!!5!1!!!068.9*Z*K+-,H.34A:*/:U!!!!.!!!!!!!!!!!!!!!!!!!!!!!!!)$`````A!!!!9!!!!'!!!!"A!!!!9!!!!'!!!!"A!!!!9!!!!'!!!!"A!!!!9!!!!'!!!!"A0!!!90]!!'0`Q!"P``!!&lt;``Q!'``]!"P``!!&lt;``Q!'``]!"P``!!&lt;``Q!'``]!"P``!!&lt;``Q!'@`Y!"B`Y!!9(Y!!'!9!!"`````Q!!!A$`````````````````````]!!!!!!!!!!!!!!!!!!!$`!!!!!!!!!!!!!!!!!!!!`Q!!!!!!!!!!!!!!!!!!!0]!!!!!!!!!!!!!!!!!!!$`!!!!!!!!!!!!!!!!!!!!`Q!!!!!!!!!!!!!!!!!!!0]!!!!!!!!!!!!!!!!!!!$`!!!!!!!!!!!!!!!!!!!!`Q!!!!!!!!!!!!!!!!!!!0]!!!!!!!!!!!!!!!!!!!$`!!!!!!!!!!!!!!!!!!!!`Q!!!!!T!!!!!!!!!!!!!0]!!!!`L`-!!!!!!!!!!!$`!!!`KKKP]Q!!!!!!!!!!`Q!`KKKKKK`T!!!!!!!!!0]0KKKKKKKKL`!!!!!!!!$`#KKKKKKKKK`Q!!!!!!!!`QL`KKKKKK``I!!!!!!!!0]+``_KKK```[!!!!!!!!$`#P```[````_A!!!!!!!!`QL`````````I!!!!!!!!0]+`````````[!!!!!!!!$`#P````````_A!!!!!!!!`QL`````````I!!!!!!!!0]+`````````[!!!!!!!!$`$``````````Q!!!!!!!!`Q#K```````[!!!!!!!!!0]!!+`````[!!!!!!!!!!$`!!!!L``[!!!!!!!!!!!!`Q!!!!#K!!!!!!!!!!!!!0`````````````````````Q!!"!$```````````````````````````````````````````]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!6&amp;1!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!6L'3)L"5!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!6L'2!1%"!C+Q6!!!!!!!!!!!!!!!!!!!!!0``!!!6L'2!1%"!1%"!1)CM&amp;1!!!!!!!!!!!!!!!!!!``]!C'2!1%"!1%"!1%"!1%#)L!!!!!!!!!!!!!!!!!$``Q"E:%"!1%"!1%"!1%"!10_)!!!!!!!!!!!!!!!!!0``!'3)C'2!1%"!1%"!10```W1!!!!!!!!!!!!!!!!!``]!:)C)C)BE1%"!10``````:!!!!!!!!!!!!!!!!!$``Q"EC)C)C)C):+T```````^E!!!!!!!!!!!!!!!!!0``!'3)C)C)C)C)`````````W1!!!!!!!!!!!!!!!!!``]!:)C)C)C)C)D`````````:!!!!!!!!!!!!!!!!!$``Q"EC)C)C)C)C0````````^E!!!!!!!!!!!!!!!!!0``!'3)C)C)C)C)`````````W1!!!!!!!!!!!!!!!!!``]!:)C)C)C)C)D`````````:!!!!!!!!!!!!!!!!!$``Q#)C)C)C)C)C0```````YC)!!!!!!!!!!!!!!!!!0``!!"E:)C)C)C)`````YCM:!!!!!!!!!!!!!!!!!!!``]!!!!!:)C)C)D``YC):!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!'3)C)C)1!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!"E1!!!!!!!!!!!!!!!!!!!!!!!!!!!````````````````````````````````````````````!!!!!A!"!!!!!!!-!!&amp;'5%B1!!!!!!!$!!!!"!!!!!!!!!54!!!-L8C=L6&gt;&gt;4&amp;NF'([`1YOH5-*JB5'495NXQ0F46IC-Q1:OY_$#:-D7I@N2N+/&gt;ED'+N)#*S6D##:',?3%&lt;79R,P##988F"&amp;G_Y-+:[=SZ%,D2O*NX)9G*CV"MSADM=X_]\04X^A5+)&lt;8*SWHT0_T\@]TT@?VI!_VWBENO!;SI1926POF1I#CE%)/\D)@GKH1+BF`Q,J-R&amp;6$D+^QI0O!WS6Q6\3+HF`&gt;)U`)/LN9@;:X#,%/%2,CU58&amp;CM3)83E&amp;,B/#EG"0(LP?+UV;DKA#LB"NHATIHO*`Q./9).1&gt;Z0LQY@W1!C66MMML=H/"372@KNT=?\7%G&lt;#I+EF)S)C??R)L&lt;_DJ5E@4"()E:*Q*,\97&amp;BQ11Z&gt;&amp;!NIX')9O;Q\$X3FQ@DF*3KK*BYG7'+'!&lt;\X$0[3/[.'=K&gt;AL+BTUK+A&amp;$%@72M?=\I*&lt;H6IQSXML+#/,QG=2_I5#9GDP-O`F(I0CEZ,8]$"%D]86\\2&lt;M,XZ-_2T&gt;VA;VVIB'C1GJ;C"$!DQ%68J16\DV)+@Q&amp;T$!H,)942^!*]BJTQC:.#W";-&lt;/.&amp;:)F\KVK(RS.RM)DHMBF4`^A-"LV$)]-D!6D95]I'!PGGN1K+&lt;9G+A"NRP)"Z7#"WR".&amp;TQ#]`0TK!&amp;?47A&lt;1CP%2!LH9,M:4YE?-E7H85XR8E8RJ,7T\V-";VIY)\9=CWU(ORYTQ^O!Y85)W?1SQNPY`Y@X)'&lt;K7F:Y)1+4=$V0%*NU5&amp;JY*R&amp;V%`)&amp;`B"C*D,#/YF^&lt;BJ^]I7X/4?]ER$*#O`M\'Q'$P6P39881IA?8HF*?[I^J2&amp;_L0U"8]'H,-)=1`3C(3'&amp;]R$;J"[&lt;D'!4KP]TBPYH.YUM11-C/YCM%RXQN%?O$A_'-;9&gt;9_'B7+Y@:S3&amp;LR=4"$ME(3[$!HA&amp;^BH&lt;@&gt;S9KWV!"Q6.U(/Q$YZ!_V:_&amp;+P1KR_)D&amp;1@A[ZU"RRO8:OE0X^3M([P#&lt;T,ZD9*8*15V_(-U\5(L-DAB%&amp;&lt;=WTF\NO35NEM*FQG?9@W/`CR=:,]8ZMCU&gt;^XU._;]V"T!;3VEH\^F"815U9H5)]+6U*+I@#R1?9!(-ZU]U.[GLITX.4_:OGKWY'&lt;@%]E/B!&lt;C!RF?@B1B:'19H%VC)GC.$EM_#[(BCQZVN@8=60,S]N[:&lt;E;0L&amp;QMEC`M@HE7%W&gt;`X+SL&amp;W&amp;+)L=*C9Y6N80['L;+B1D,A+,CYN9#;_G;17?O&amp;M`,8;QSR[WJ*J?"7`3U8\?KXO;P:1?JGJ[T6S;MBT:R*!.(L!#EQW5)"P8\NF=WDG&lt;3RFM--WD/JN3X=JP4=7,E-WP^X^$.K.48T)W6C0%;XYTU'M(UO\LUOZ^ZPW4HT*$0Y9^UZ],^/7#A^M`&amp;]&lt;:=_&amp;-M:Z93_U5._7ZDCOGKC@1U6IP])(E9BR*(=:)9NGMBW"GC&amp;`@]CE;X%')3Q0D!]0B5_'BU;XG56@/0#L(?&gt;39@R[&gt;SJF(&lt;JR(L8HH58@70()CE?/\H5&gt;PZ-[D#BQ"L4O:2TUZ]]C*][B_"`0I^#&lt;TS'L-)\4SL+4YGQUO*_!&amp;ZK46=0,.4==2^;-N^;C\N&lt;76.O:C:SR].&gt;@%NX",[%=!+\.T#BTYQ*P(D(/I8V.7N0UA\M[-]\FG70(^%LCX-1-FP:!O[56&gt;UE+5F*Y17X@;$PA(0YSG`C$Q!6(3]CU1/L$]B!LF5LPT2[%4&gt;W,H/`E/Y1\_&amp;-.@:8PY/`RK`/@58YBY6QIJ,Z%L:%H]X-V8`A&gt;#PK-(!!!!!!1!!!"%!!!!"!!!!!!!!!!-!!&amp;#2%B1!!!!!!!$!!!!"!!!!!!!!!"C!!!!=HC=9W"AS"/190L(50?8A5HA+Z!B`:?"7&gt;#0]4=$![?@Q'%AT3AA#237`=P!,KA.&amp;N9_IMP"!!7K&lt;)Q=EBS("4H!-BQN'AT````H_(LE'FT&amp;%2]Y5W770)=%!"2C'1!!!!!!!!1!!!!(!!!&gt;#Q!!!!A!!!!.4EF@37.P&lt;E6E;82P=A!!':=A!)!!!!!!!1!/1$$`````"%2B&gt;'%!!!%!!!!!'85S-$!R/$!Q-!U!!!!!!2=64'^B:#!G)&amp;6O&lt;'^B:#ZM&gt;G.M98.T!!!"!!!!!!!*!!!:1Q&amp;E!7216%AQ!!!!"!!!!!!!!!Q?!"U!!!Q9!!!-!!!!!!!!)!!A!"A!!!!!!0```Q!!````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````!!!!!1!!!!%0$5RB?76S,GRW9WRB=X-!!!%!!!!!!!=!!!SZ!!!!!!!!!!!!!!S?!#A!!!S9!!!-!!!!!!!!)!!A!"A!!!!!!0```Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[!!!!!!!!_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[!!!!!!!!_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[!!!!!!!!_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[!!!!!!!!_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[!!!!!!!!_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[!!!!!!!!_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[!!!!!!!!_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[!!!!!!!!_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[!!!!!!!!_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[!!!!!!!!_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[!!!!!!!!_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[`W:G`W:G_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[!!!!!!!!_PL[_PL[_PL[_PL[_PL[_PL[`W:G1"I;A$-T63)C1"I;`W:G_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[!!!!!!!!_PL[_PL[_PL[_PL[`W:G1"I;A$-TPUR-PUR-PUR-PUR-63)C1"I;`W:G_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[!!!!!!!!_PL[_PL[`W:G1"I;A$-TPUR-PUR-PUR-PUR-PUR-PUR-PUR-PUR-63)C1"I;`W:G_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[!!!!!!!!_PL[63)CA$-TPUR-PUR-PUR-PUR-PUR-PUR-PUR-PUR-PUR-PUR-PUR-PUR-63)C1"I;_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[!!!!!!!!_PL[A$-TA$-TPUR-PUR-PUR-PUR-PUR-PUR-PUR-PUR-PUR-PUR-PUR-PUR-&amp;1A)63)C_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[!!!!!!!!_PL[A$-T63)C63)CA$-TPUR-PUR-PUR-PUR-PUR-PUR-PUR-PUR-&amp;1A)&amp;1A)&amp;1A)A$-T_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[!!!!!!!!_PL[A$-T63)C63)C63)C63)CA$-TPUR-PUR-PUR-PUR-&amp;1A)&amp;1A)&amp;1A)&amp;1A)&amp;1A)A$-T_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[!!!!!!!!_PL[A$-T63)C63)C63)C63)C63)C63)CA$-T1"I;&amp;1A)&amp;1A)&amp;1A)&amp;1A)&amp;1A)&amp;1A)A$-T_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[!!!!!!!!_PL[A$-T63)C63)C63)C63)C63)C63)C63)C&amp;1A)&amp;1A)&amp;1A)&amp;1A)&amp;1A)&amp;1A)&amp;1A)A$-T_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[!!!!!!!!_PL[A$-T63)C63)C63)C63)C63)C63)C63)C&amp;1A)&amp;1A)&amp;1A)&amp;1A)&amp;1A)&amp;1A)&amp;1A)A$-T_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[!!!!!!!!_PL[A$-T63)C63)C63)C63)C63)C63)C63)C&amp;1A)&amp;1A)&amp;1A)&amp;1A)&amp;1A)&amp;1A)&amp;1A)A$-T_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[!!!!!!!!_PL[A$-T63)C63)C63)C63)C63)C63)C63)C&amp;1A)&amp;1A)&amp;1A)&amp;1A)&amp;1A)&amp;1A)&amp;1A)A$-T_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[!!!!!!!!_PL[A$-T63)C63)C63)C63)C63)C63)C63)C&amp;1A)&amp;1A)&amp;1A)&amp;1A)&amp;1A)&amp;1A)&amp;1A)A$-T_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[!!!!!!!!_PL[63)C63)C63)C63)C63)C63)C63)C63)C&amp;1A)&amp;1A)&amp;1A)&amp;1A)&amp;1A)&amp;1A)63)C63)C_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[!!!!!!!!_PL[_PL[F4Q]A$-T63)C63)C63)C63)C63)C&amp;1A)&amp;1A)&amp;1A)&amp;1A)63)C1"I;F4Q]_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[!!!!!!!!_PL[_PL[_PL[_PL[F4Q]63)C63)C63)C63)C&amp;1A)&amp;1A)63)C63)CF4Q]_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[!!!!!!!!_PL[_PL[_PL[_PL[_PL[_PL[F4Q]63)C63)C63)C63)CPUR-_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[!!!!!!!!_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[F4Q]PUR-_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[_PL[!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````]!!!!(6EEA37.P&lt;G1"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!N4&lt;7&amp;M&lt;#"'&lt;WZU=Q!"#!%"!!!!!!!!!#&amp;@&lt;GF@4'&amp;T&gt;%NO&lt;X&gt;O4X&gt;O;7ZH4&amp;:$&lt;'&amp;T=U.M&gt;8.U:8)!!!#D)!#!!!!!!!%!#!!Q`````Q!"!!!!!!#(!!!!"1!71(!!#!!!!!9!!!F.:7ZV)%FU:7U!'E"Q!"E!!1!!$V.X;8"F476O&gt;3"&amp;&gt;G6O&gt;!!01!-!#&amp;"P=WFU;7^O!!!?1(!!'1!"!!)35'&amp;O)%.P&lt;8"M:82F)%6W:7ZU!!!C1&amp;!!!A!"!!-85X&gt;J='5A5W.S:76O)&amp;6*,GRW9WRB=X-!!1!%!!!!!!!!!"J-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;597*0=G2F=A!!!#UA!)!!!!!!!A!&amp;!!=!!!Q!1!!"`````Q!!!!%!!1!!!!)!!!!!!!!!!1!!!!!!!!!&lt;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B6'FN:8.U97VQ!!!!'3!!A!!!!!!"!!5!"Q!!!1!!XO@[\Q!!!!!!!!!G4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B4'&amp;T&gt;%&amp;Q='RJ:725;7VF=X2B&lt;8!!!!!:)!#!!!!!!!%!"1!(!!!"!!$?Z`LP!!!!!!!!!"J-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;5?8"F2'6T9Q!!!+-A!)!!!!!!!1!)!$$`````!!%!!!!!!)=!!!!&amp;!":!=!!)!!!!"A!!#5VF&lt;H5A382F&lt;1!;1(!!'1!"!!!05X&gt;J='6.:7ZV)%6W:7ZU!!^!!Q!)5'^T;82J&lt;WY!!"Z!=!!:!!%!!B*197YA1W^N='RF&gt;'5A28:F&lt;H1!!#*!5!!#!!%!!R&gt;4&gt;WFQ:3"49X*F:7YA65EO&lt;(:D&lt;'&amp;T=Q!"!!1!!!!!!!!!(ER71WRB=X.1=GFW982F2'&amp;U952G&lt;(2%982B5WF[:1!!!"EA!)!!!!!!!1!&amp;!!-!!!%!!!!!!!A!!!!!!!!!'ER71WRB=X.1=GFW982F2'&amp;U952G&lt;(2%982B!!!!FS!!A!!!!!!&amp;!":!=!!)!!!!"A!!#5VF&lt;H5A382F&lt;1!;1(!!'1!"!!!05X&gt;J='6.:7ZV)%6W:7ZU!!^!!Q!)5'^T;82J&lt;WY!!"Z!=!!:!!%!!B*197YA1W^N='RF&gt;'5A28:F&lt;H1!!#*!5!!#!!%!!R&gt;4&gt;WFQ:3"49X*F:7YA65EO&lt;(:D&lt;'&amp;T=Q!"!!1!!!!!!!!!!!!!!!!!!!!!"!!'!!Q!!!!%!!!!&gt;Q!!!#A!!!!#!!!%!!!!!!1!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"0A!!!B6YH)V105`$1!R^S&gt;%GA&gt;+0&amp;&amp;J;",K2!&lt;(Q"S*6)(6!CKD9#=E&amp;2&lt;IG58-.D0R36E:A:=+ZB)*A1&gt;&lt;Z\'@L0&gt;M!$D(S=NA!WI"T*&gt;)VHSORR*41#1SANXB)=K%,&amp;[6)&amp;8I?A_VH2;+3,!7/GU&lt;4^9/5T\*F,I5343]+P$_`@,Q308-V%6_%+S(3-VH+Z'\]%_)X=U*$'24&amp;]$=?+IF4TY&gt;*1ORI*N?&amp;%CO?R6SX]XS6F!'*2I%+U#,$.C,!?.-*;V+=QY,D77#RP)@N0&lt;(M7N7!S',UC:`"I&gt;Y&gt;NIZC&gt;,#Z#W(VELO&lt;S%*8T^.&amp;D[H(C0!K[[#0EX`0;9*=OX*U:ROX*,3&amp;3S,C''*0T^\#0EF^7;ORP]BX:;3N8ND!G+*1]RLUA!(Z.GE.Y/*!,T+BJ;?5O@289_-47UN;I1!!!!!!:1!"!!)!!Q!%!!!!3!!0"!!!!!!0!.E!V!!!!&amp;%!$Q1!!!!!$Q$:!.1!!!";!!]%!!!!!!]!W1$5!!!!9Y!!B!#!!!!0!.E!V!B4:7&gt;P:3"631B4:7&gt;P:3"631B4:7&gt;P:3"631%Q!!!!5F.31QU+!!.-6E.$4%*76Q!!,T1!!!3=!!!!)!!!,R1!!!!!!!!!!!!!!#!!!!!U!!!%C!!!!"^-35*/!!!!!!!!!92-6F.3!!!!!!!!!:B36&amp;.(!!!!!!!!!;R$1V.5!!!!!!!!!="-38:J!!!!!!!!!&gt;2$4UZ1!!!!!!!!!?B544AQ!!!!!!!!!@R%2E24!!!!!!!!!B"-372T!!!!!!!!!C2735.%!!!!!!!!!DBW:8*T!!!!"!!!!ER41V.3!!!!!!!!!L"(1V"3!!!!!!!!!M2*1U^/!!!!!!!!!NBJ9WQU!!!!!!!!!ORJ9WQY!!!!!!!!!Q"$5%-S!!!!!!!!!R2-37:Q!!!!!!!!!SB'5%6Y!!!!!!!!!TR'5%BC!!!!!!!!!V"'5&amp;.&amp;!!!!!!!!!W275%21!!!!!!!!!XB-37*E!!!!!!!!!YR#2%6Y!!!!!!!!!["#2%BC!!!!!!!!!\2#2&amp;.&amp;!!!!!!!!!]B73624!!!!!!!!!^R%6%B1!!!!!!!!!`".65F%!!!!!!!!"!2)36.5!!!!!!!!""B71V21!!!!!!!!"#R'6%&amp;#!!!!!!!!"%!!!!!!`````Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$1!!!!!!!!!!0````]!!!!!!!!!W!!!!!!!!!!!`````Q!!!!!!!!$M!!!!!!!!!!$`````!!!!!!!!!01!!!!!!!!!!0````]!!!!!!!!")!!!!!!!!!!!`````Q!!!!!!!!%I!!!!!!!!!!$`````!!!!!!!!!61!!!!!!!!!!0````]!!!!!!!!"J!!!!!!!!!!!`````Q!!!!!!!!'U!!!!!!!!!!4`````!!!!!!!!!T1!!!!!!!!!"`````]!!!!!!!!$3!!!!!!!!!!)`````Q!!!!!!!!.9!!!!!!!!!!H`````!!!!!!!!!WQ!!!!!!!!!#P````]!!!!!!!!$@!!!!!!!!!!!`````Q!!!!!!!!/1!!!!!!!!!!$`````!!!!!!!!![A!!!!!!!!!!0````]!!!!!!!!$P!!!!!!!!!!!`````Q!!!!!!!!2!!!!!!!!!!!$`````!!!!!!!!"E1!!!!!!!!!!0````]!!!!!!!!+3!!!!!!!!!!!`````Q!!!!!!!!J1!!!!!!!!!!$`````!!!!!!!!#G!!!!!!!!!!!0````]!!!!!!!!+;!!!!!!!!!!!`````Q!!!!!!!!_!!!!!!!!!!!$`````!!!!!!!!$YA!!!!!!!!!!0````]!!!!!!!!0E!!!!!!!!!!!`````Q!!!!!!!!_A!!!!!!!!!!$`````!!!!!!!!$[A!!!!!!!!!!0````]!!!!!!!!1%!!!!!!!!!!!`````Q!!!!!!!"!9!!!!!!!!!!$`````!!!!!!!!,3A!!!!!!!!!!0````]!!!!!!!!N-!!!!!!!!!!!`````Q!!!!!!!#UY!!!!!!!!!!$`````!!!!!!!!,71!!!!!!!!!A0````]!!!!!!!!OK!!!!!!45X&gt;J='5A5W.S:76O)&amp;6*,G.U&lt;!!!!!!</Property>
	<Property Name="NI.LVClass.Geneology" Type="Xml"><String>
<Name></Name>
<Val>!!!!!B*4&gt;WFQ:3"49X*F:7YO&lt;(:M;7)85X&gt;J='5A5W.S:76O)&amp;6*,GRW9WRB=X-!5&amp;2)-!!!!!!!!!!!!!!!"1!"!!!!!!!!!1!!!!%!&amp;E"1!!!05X&gt;J='5A5W.S:76O)&amp;6*!!%!!!!!!!!!!!!!!1Z-97*73568)%^C;G6D&gt;!"16%AQ!!!!!!!!!!!!)!#!!!!!!!!!!!(``Q!!!!%!!!!!!!%"!!!!!1!71&amp;!!!!^4&gt;WFQ:3"49X*F:7YA65E!!1!!!!!!!@````Y!!!!!!2&gt;633"):7RQ:8)A1WRB=X-O&lt;(:D&lt;'&amp;T=V"53$!!!!!!!!!!!!!A!)!!!!!!!!!!!!!!!!!!!1!!!!!!!!!!!!!"!":!5!!!$V.X;8"F)&amp;.D=G6F&lt;C"631!"!!!!!!!"`````A!!!!!"&amp;V6*)%BF&lt;("F=C"$&lt;'&amp;T=SZM&gt;G.M98.T5&amp;2)-!!!!!!!!!!!!#!!A!!!!!!!!!!!!!!!!!!"!!!!!!!"!!!!!!-!&amp;E"Q!!A!!!!'!!!*476O&gt;3"*&gt;'6N!"J!=!!:!!%!!!^4&gt;WFQ:5VF&lt;H5A28:F&lt;H1!=1$RXO;FX1!!!!-35X&gt;J='5A5W.S:76O,GRW&lt;'FC&amp;V.X;8"F)&amp;.D=G6F&lt;C"633ZM&gt;G.M98.T%V.X;8"F)&amp;.D=G6F&lt;C"633ZD&gt;'Q!+E"1!!%!!2V$&lt;(6T&gt;'6S)'^G)'.M98.T)("S;8:B&gt;'5A:'&amp;U91!"!!)!!!!"`````Q!!!!!!!!!!!!%865EA3'6M='6S)%.M98.T,GRW9WRB=X.16%AQ!!!!!!!!!!!!)!#!!!!!!!!!!!!!!!!!!!%!!!!!!!)!!!!!"1!71(!!#!!!!!9!!!F.:7ZV)%FU:7U!'E"Q!"E!!1!!$V.X;8"F476O&gt;3"&amp;&gt;G6O&gt;!!01!-!#&amp;"P=WFU;7^O!!!?1(!!'1!"!!)35'&amp;O)%.P&lt;8"M:82F)%6W:7ZU!!"T!0(?Z`LP!!!!!R*4&gt;WFQ:3"49X*F:7YO&lt;(:M;7)85X&gt;J='5A5W.S:76O)&amp;6*,GRW9WRB=X-45X&gt;J='5A5W.S:76O)&amp;6*,G.U&lt;!!M1&amp;!!!A!"!!-&gt;1WRV=X2F=C"P:C"D&lt;'&amp;T=S"Q=GFW982F)'2B&gt;'%!!1!%!!!!!A!!!!$`````!!!!!!!!!!!!!!!!!!!"&amp;V6*)%BF&lt;("F=C"$&lt;'&amp;T=SZM&gt;G.M98.T5&amp;2)-!!!!!!!!!!!!#!!A!!!!!!!!!!!!!!"!!!!&amp;V.X;8"F)&amp;.D=G6F&lt;C"633ZM&gt;G.M98.T</Val>
</String>
</Property>
	<Property Name="NI.LVClass.IsTransferClass" Type="Bool">false</Property>
	<Property Name="NI.LVClass.LowestCompatibleVersion" Type="Str">1.0.0.0</Property>
	<Property Name="NI_IconEditor" Type="Str">50 48 48 49 56 48 48 48 13 0 0 0 0 1 23 21 76 111 97 100 32 38 32 85 110 108 111 97 100 46 108 118 99 108 97 115 115 0 0 1 0 0 0 0 0 9 0 0 5 95 1 100 0 0 80 84 72 48 0 0 0 4 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 15 13 76 97 121 101 114 46 108 118 99 108 97 115 115 0 0 1 0 0 0 0 0 7 0 0 4 236 0 0 0 0 0 0 0 0 0 0 4 206 0 40 0 0 4 200 0 0 4 128 0 0 0 0 0 12 0 32 0 24 0 0 0 0 0 255 255 255 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 0 0 0 0 0 0 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 0 0 0 0 0 0 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 0 0 0 0 0 0 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 0 0 0 0 0 0 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 0 0 0 0 0 0 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 0 0 0 0 0 0 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 0 0 0 0 0 0 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 0 0 0 0 0 0 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 0 0 0 0 0 0 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 255 102 102 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 10 78 73 95 76 105 98 114 97 114 121 100 1 0 0 0 0 0 7 83 119 105 112 101 85 73 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 11 83 109 97 108 108 32 70 111 110 116 115 0 1 8 1 1

</Property>
	<Item Name="Parent Libraries" Type="Parent Libraries">
		<Item Name="UI Helper Class.lvclass" Type="Parent" URL="../../../UI Helper Class/UI Helper Class.lvclass"/>
	</Item>
	<Item Name="Swipe Screen UI.ctl" Type="Class Private Data" URL="Swipe Screen UI.ctl">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="Custom Controls" Type="Folder">
		<Item Name="Action Checkbox.ctl" Type="VI" URL="../Action Checkbox.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!7!!!!!1!/1#%)1WBF9WNC&lt;XA!!!%!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082143232</Property>
		</Item>
		<Item Name="GCentral Button.ctl" Type="VI" URL="../GCentral Button.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"@!!!!!1"8!0(?ZMB!!!!!!R*4&gt;WFQ:3"49X*F:7YO&lt;(:M;7)85X&gt;J='5A5W.S:76O)&amp;6*,GRW9WRB=X-42U.F&lt;H2S97QA1H6U&gt;'^O,G.U&lt;!!11#%(4'&amp;C6EF&amp;6Q!"!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">3145728</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1074278912</Property>
		</Item>
		<Item Name="GDevCon Button.ctl" Type="VI" URL="../GDevCon Button.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"?!!!!!1"7!0(:ET@C!!!!!R*4&gt;WFQ:3"49X*F:7YO&lt;(:M;7)85X&gt;J='5A5W.S:76O)&amp;6*,GRW9WRB=X-32U2F&gt;E.P&lt;C"#&gt;82U&lt;WYO9X2M!""!)1&gt;-97*73568!!%!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">3145728</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1074278912</Property>
		</Item>
		<Item Name="GDevCon NA Button.ctl" Type="VI" URL="../GDevCon NA Button.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"B!!!!!1":!0(?ZM;L!!!!!R*4&gt;WFQ:3"49X*F:7YO&lt;(:M;7)85X&gt;J='5A5W.S:76O)&amp;6*,GRW9WRB=X-62U2F&gt;E.P&lt;C"/13"#&gt;82U&lt;WYO9X2M!""!)1&gt;(2'6W1W^O!!%!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">3145728</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1074278912</Property>
		</Item>
		<Item Name="GIE Button.ctl" Type="VI" URL="../GIE Button.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"=!!!!!1"5!0(?ZMBR!!!!!R*4&gt;WFQ:3"49X*F:7YO&lt;(:M;7)85X&gt;J='5A5W.S:76O)&amp;6*,GRW9WRB=X-/2UF&amp;)%*V&gt;(2P&lt;CZD&gt;'Q!%E!B#%&gt;$:7ZU=G&amp;M!!!"!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">3145728</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1074278912</Property>
		</Item>
		<Item Name="GLA Button.ctl" Type="VI" URL="../GLA Button.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"A!!!!!1"9!0(?ZM@*!!!!!R*4&gt;WFQ:3"49X*F:7YO&lt;(:M;7)85X&gt;J='5A5W.S:76O)&amp;6*,GRW9WRB=X-/2UR")%*V&gt;(2P&lt;CZD&gt;'Q!&amp;E!B$%RB9F:*26=A6WFL;1!!!1!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">3145728</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1074278912</Property>
		</Item>
		<Item Name="LabVIEW Button.ctl" Type="VI" URL="../LabVIEW Button.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"&lt;!!!!!1"4!0(:ET@C!!!!!R*4&gt;WFQ:3"49X*F:7YO&lt;(:M;7)85X&gt;J='5A5W.S:76O)&amp;6*,GRW9WRB=X-06'6T&gt;#"#&gt;82U&lt;WYO9X2M!""!)1&gt;-97*73568!!%!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">3145728</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1074278912</Property>
		</Item>
		<Item Name="LabVIEW Wiki Button.ctl" Type="VI" URL="../LabVIEW Wiki Button.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"D!!!!!1"&lt;!0(:ET@C!!!!!R*4&gt;WFQ:3"49X*F:7YO&lt;(:M;7)85X&gt;J='5A5W.S:76O)&amp;6*,GRW9WRB=X-84'&amp;C6EF&amp;6S"8;7NJ)%*V&gt;(2P&lt;CZD&gt;'Q!%%!B"URB9F:*26=!!1!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">3145728</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1074278912</Property>
		</Item>
		<Item Name="LAVA Button.ctl" Type="VI" URL="../LAVA Button.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"B!!!!!1":!0(?ZM@*!!!!!R*4&gt;WFQ:3"49X*F:7YO&lt;(:M;7)85X&gt;J='5A5W.S:76O)&amp;6*,GRW9WRB=X-04%&amp;713"#&gt;82U&lt;WYO9X2M!":!)1R-97*73568)&amp;&gt;J;WE!!!%!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">3145728</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1074278912</Property>
		</Item>
		<Item Name="Left Arrow Button.ctl" Type="VI" URL="../Left Arrow Button.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!7!!!!!1!/1#%)1WBF9WNC&lt;XA!!!%!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1074278912</Property>
		</Item>
		<Item Name="More Ellipsis Button.ctl" Type="VI" URL="../More Ellipsis Button.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!3!!!!!1!+1#%%47^S:1!!!1!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082143232</Property>
		</Item>
		<Item Name="NI Button.ctl" Type="VI" URL="../NI Button.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"@!!!!!1"8!0(?ZM@*!!!!!R*4&gt;WFQ:3"49X*F:7YO&lt;(:M;7)85X&gt;J='5A5W.S:76O)&amp;6*,GRW9WRB=X-.4EEA1H6U&gt;'^O,G.U&lt;!!71#%-4'&amp;C6EF&amp;6S"8;7NJ!!!"!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">3145728</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1074278912</Property>
		</Item>
		<Item Name="Picture Count Radio Cluster.ctl" Type="VI" URL="../Picture Count Radio Cluster.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!N!!!!!1!F1"=!"A%R!4)"-Q%U!45".A!25'FD&gt;(6S:3"4:7RF9X2J&lt;WY!!1!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082143232</Property>
		</Item>
		<Item Name="QSI Button.ctl" Type="VI" URL="../QSI Button.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"7!!!!!1"/!0(:ET@Y!!!!!R*4&gt;WFQ:3"49X*F:7YO&lt;(:M;7)85X&gt;J='5A5W.S:76O)&amp;6*,GRW9WRB=X-/56.*)%*V&gt;(2P&lt;CZD&gt;'Q!$%!B!V&amp;431!"!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">3145728</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1074278912</Property>
		</Item>
		<Item Name="Radio Type 1.ctl" Type="VI" URL="../Radio Type 1.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!?!!!!!1!71#%25G&amp;E;7]A5W6M:7.U;7^O)$%!!1!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082143232</Property>
		</Item>
		<Item Name="Radio Type 2.ctl" Type="VI" URL="../Radio Type 2.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"G!!!!!1"?!0(?ZHSS!!!!!R*4&gt;WFQ:3"49X*F:7YO&lt;(:M;7)85X&gt;J='5A5W.S:76O)&amp;6*,GRW9WRB=X-15G&amp;E;7]A6(FQ:3!S,G.U&lt;!!;1#%25G&amp;E;7]A5W6M:7.U;7^O)$)!!1!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">3145728</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082143232</Property>
		</Item>
		<Item Name="VIPM Button.ctl" Type="VI" URL="../VIPM Button.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"8!!!!!1"0!0(:ET@Y!!!!!R*4&gt;WFQ:3"49X*F:7YO&lt;(:M;7)85X&gt;J='5A5W.S:76O)&amp;6*,GRW9WRB=X-06EF143"#&gt;82U&lt;WYO9X2M!!R!)1.25UE!!1!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">3145728</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1074278912</Property>
		</Item>
	</Item>
	<Item Name="Private Methods" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="Initialize Action JiggleButton QControls.vi" Type="VI" URL="../Initialize Action JiggleButton QControls.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;5!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%J!=!!?!!!M%F.X;8"F)&amp;.D=G6F&lt;CZM&gt;GRJ9B&gt;4&gt;WFQ:3"49X*F:7YA65EO&lt;(:D&lt;'&amp;T=Q!!%V.X;8"F)&amp;.D=G6F&lt;C"633"P&gt;81!&amp;E"1!!-!!!!"!!)):8*S&lt;X)A;7Y!!%J!=!!?!!!M%F.X;8"F)&amp;.D=G6F&lt;CZM&gt;GRJ9B&gt;4&gt;WFQ:3"49X*F:7YA65EO&lt;(:D&lt;'&amp;T=Q!!%F.X;8"F)&amp;.D=G6F&lt;C"633"J&lt;A!!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!*!!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1115685392</Property>
		</Item>
		<Item Name="Initialize Return JiggleButton QControls.vi" Type="VI" URL="../Initialize Return JiggleButton QControls.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;5!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%J!=!!?!!!M%F.X;8"F)&amp;.D=G6F&lt;CZM&gt;GRJ9B&gt;4&gt;WFQ:3"49X*F:7YA65EO&lt;(:D&lt;'&amp;T=Q!!%V.X;8"F)&amp;.D=G6F&lt;C"633"P&gt;81!&amp;E"1!!-!!!!"!!)):8*S&lt;X)A;7Y!!%J!=!!?!!!M%F.X;8"F)&amp;.D=G6F&lt;CZM&gt;GRJ9B&gt;4&gt;WFQ:3"49X*F:7YA65EO&lt;(:D&lt;'&amp;T=Q!!%F.X;8"F)&amp;.D=G6F&lt;C"633"J&lt;A!!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!*!!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">2</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1074541072</Property>
		</Item>
		<Item Name="Initialize SimplePan QControl.vi" Type="VI" URL="../Initialize SimplePan QControl.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;5!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%J!=!!?!!!M%F.X;8"F)&amp;.D=G6F&lt;CZM&gt;GRJ9B&gt;4&gt;WFQ:3"49X*F:7YA65EO&lt;(:D&lt;'&amp;T=Q!!%V.X;8"F)&amp;.D=G6F&lt;C"633"P&gt;81!&amp;E"1!!-!!!!"!!)):8*S&lt;X)A;7Y!!%J!=!!?!!!M%F.X;8"F)&amp;.D=G6F&lt;CZM&gt;GRJ9B&gt;4&gt;WFQ:3"49X*F:7YA65EO&lt;(:D&lt;'&amp;T=Q!!%F.X;8"F)&amp;.D=G6F&lt;C"633"J&lt;A!!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1074536976</Property>
		</Item>
		<Item Name="Initialize SwipeMenu QControl.vi" Type="VI" URL="../Initialize SwipeMenu QControl.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;5!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%J!=!!?!!!M%F.X;8"F)&amp;.D=G6F&lt;CZM&gt;GRJ9B&gt;4&gt;WFQ:3"49X*F:7YA65EO&lt;(:D&lt;'&amp;T=Q!!%V.X;8"F)&amp;.D=G6F&lt;C"633"P&gt;81!&amp;E"1!!-!!!!"!!)):8*S&lt;X)A;7Y!!%J!=!!?!!!M%F.X;8"F)&amp;.D=G6F&lt;CZM&gt;GRJ9B&gt;4&gt;WFQ:3"49X*F:7YA65EO&lt;(:D&lt;'&amp;T=Q!!%F.X;8"F)&amp;.D=G6F&lt;C"633"J&lt;A!!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1074536980</Property>
		</Item>
	</Item>
	<Item Name="Reference Accessors" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="Action JiggleButton Ref" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">Action JiggleButton Ref</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Action JiggleButton Ref</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="Get Action Jiggle QControl Reference.vi" Type="VI" URL="../Get Action Jiggle QControl Reference.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'?!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#Z!=!!?!!!7&amp;%JJ:W&gt;M:5*V&gt;(2P&lt;CZM&gt;G.M98.T!!!-3GFH:WRF1H6U&gt;'^O!!!=1%!!!@````]!"1Z"9X2J&lt;WYA1H6U&gt;'^O=Q!!3E"Q!"Y!!#Q35X&gt;J='5A5W.S:76O,GRW&lt;'FC&amp;V.X;8"F)&amp;.D=G6F&lt;C"633ZM&gt;G.M98.T!!!45X&gt;J='5A5W.S:76O)&amp;6*)'^V&gt;!!71&amp;!!!Q!!!!%!!ABF=H*P=C"J&lt;A!!3E"Q!"Y!!#Q35X&gt;J='5A5W.S:76O,GRW&lt;'FC&amp;V.X;8"F)&amp;.D=G6F&lt;C"633ZM&gt;G.M98.T!!!35X&gt;J='5A5W.S:76O)&amp;6*)'FO!!"B!0!!$!!$!!1!"A!(!!1!"!!%!!1!#!!%!!1!#1-!!(A!!!E!!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!+!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
			</Item>
		</Item>
		<Item Name="Actions Ref" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">Actions Ref</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Actions Ref</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="Get Actions Reference.vi" Type="VI" URL="../Get Actions Reference.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'\!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%=!&amp;Q!($%ZP)&amp;.F&lt;'6D&gt;'FP&lt;A2.&lt;X:F#5^W:8*T;'^P&gt;!2%=G&amp;H#&amp;.M&lt;X&gt;E&lt;X&gt;O#F.Q=GFO:W*B9WM*5W6M:7.U;7^O!!!!)%"Q!!A!!1!&amp;!%A!!"&amp;"9X2J&lt;WZT)&amp;*F:G6S:7ZD:1"+1(!!(A!!,"*4&gt;WFQ:3"49X*F:7YO&lt;(:M;7)85X&gt;J='5A5W.S:76O)&amp;6*,GRW9WRB=X-!!".4&gt;WFQ:3"49X*F:7YA65EA&lt;X6U!":!5!!$!!!!!1!##'6S=G^S)'FO!!"+1(!!(A!!,"*4&gt;WFQ:3"49X*F:7YO&lt;(:M;7)85X&gt;J='5A5W.S:76O)&amp;6*,GRW9WRB=X-!!"*4&gt;WFQ:3"49X*F:7YA65EA;7Y!!'%!]!!-!!-!"!!'!!=!"!!%!!1!"!!)!!1!"!!*!Q!!?!!!$1A!!!!!!!!*!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!1!!!.!!!!$!!!!!!!!!!!!!!"!!I!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
			</Item>
		</Item>
		<Item Name="Code View Ref" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">Code View Ref</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Code View Ref</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="Get Code View Reference.vi" Type="VI" URL="../Get Code View Reference.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;U!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#"!=!!)!!!!$Q!!%U.P:'5A6GFF&gt;S"3:7:F=G6O9W5!3E"Q!"Y!!#Q35X&gt;J='5A5W.S:76O,GRW&lt;'FC&amp;V.X;8"F)&amp;.D=G6F&lt;C"633ZM&gt;G.M98.T!!!45X&gt;J='5A5W.S:76O)&amp;6*)'^V&gt;!!71&amp;!!!Q!!!!%!!ABF=H*P=C"J&lt;A!!3E"Q!"Y!!#Q35X&gt;J='5A5W.S:76O,GRW&lt;'FC&amp;V.X;8"F)&amp;.D=G6F&lt;C"633ZM&gt;G.M98.T!!!35X&gt;J='5A5W.S:76O)&amp;6*)'FO!!"B!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!-!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
			</Item>
		</Item>
		<Item Name="Code View SimplePan Ref" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">Code View SimplePan Ref</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Code View SimplePan Ref</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="Get Code View SimplePan QControl Reference.vi" Type="VI" URL="../Get Code View SimplePan QControl Reference.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'9!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%2!=!!?!!!4%6.J&lt;8"M:6"B&lt;CZM&gt;G.M98.T!#:$&lt;W2F)&amp;:J:8=A5WFN='RF5'&amp;O)&amp;&amp;D&lt;WZU=G^M)&amp;*F:G6S:7ZD:1!!3E"Q!"Y!!#Q35X&gt;J='5A5W.S:76O,GRW&lt;'FC&amp;V.X;8"F)&amp;.D=G6F&lt;C"633ZM&gt;G.M98.T!!!45X&gt;J='5A5W.S:76O)&amp;6*)'^V&gt;!!71&amp;!!!Q!!!!%!!ABF=H*P=C"J&lt;A!!3E"Q!"Y!!#Q35X&gt;J='5A5W.S:76O,GRW&lt;'FC&amp;V.X;8"F)&amp;.D=G6F&lt;C"633ZM&gt;G.M98.T!!!35X&gt;J='5A5W.S:76O)&amp;6*)'FO!!"B!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!-!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342972432</Property>
			</Item>
		</Item>
		<Item Name="Menu Subpanel Ref" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">Menu Subpanel Ref</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Menu Subpanel Ref</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="Get Menu Subpanel Reference.vi" Type="VI" URL="../Get Menu Subpanel Reference.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;Y!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#2!=!!)!!!!11!!&amp;UVF&lt;H5A5X6C='&amp;O:7QA5G6G:8*F&lt;G.F!%J!=!!?!!!M%F.X;8"F)&amp;.D=G6F&lt;CZM&gt;GRJ9B&gt;4&gt;WFQ:3"49X*F:7YA65EO&lt;(:D&lt;'&amp;T=Q!!%V.X;8"F)&amp;.D=G6F&lt;C"633"P&gt;81!&amp;E"1!!-!!!!"!!)):8*S&lt;X)A;7Y!!%J!=!!?!!!M%F.X;8"F)&amp;.D=G6F&lt;CZM&gt;GRJ9B&gt;4&gt;WFQ:3"49X*F:7YA65EO&lt;(:D&lt;'&amp;T=Q!!%F.X;8"F)&amp;.D=G6F&lt;C"633"J&lt;A!!91$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!"!!%!!A$!!"Y!!!.#!!!!!!!!!E!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!"!!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342972432</Property>
			</Item>
		</Item>
		<Item Name="Picture Selection Ref" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">Picture Selection Ref</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Picture Selection Ref</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="Get Picture Selection Reference.vi" Type="VI" URL="../Get Picture Selection Reference.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'2!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"-!&amp;Q!'!4%"-A%T!41".1%W!!!K1(!!#!!"!!5!3!!!'V"J9X2V=G5A5W6M:7.U;7^O)&amp;*F:G6S:7ZD:1"+1(!!(A!!,"*4&gt;WFQ:3"49X*F:7YO&lt;(:M;7)85X&gt;J='5A5W.S:76O)&amp;6*,GRW9WRB=X-!!".4&gt;WFQ:3"49X*F:7YA65EA&lt;X6U!":!5!!$!!!!!1!##'6S=G^S)'FO!!"+1(!!(A!!,"*4&gt;WFQ:3"49X*F:7YO&lt;(:M;7)85X&gt;J='5A5W.S:76O)&amp;6*,GRW9WRB=X-!!"*4&gt;WFQ:3"49X*F:7YA65EA;7Y!!'%!]!!-!!-!"!!'!!=!"!!%!!1!"!!)!!1!"!!*!Q!!?!!!$1A!!!!!!!!*!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!1!!!.!!!!$!!!!!!!!!!!!!!"!!I!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">33554432</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
			</Item>
		</Item>
		<Item Name="Return to Origin JiggleButton Ref" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">Return to Origin JiggleButton Ref</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Return to Origin JiggleButton Ref</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="Get Return Jiggle QControl Reference.vi" Type="VI" URL="../Get Return Jiggle QControl Reference.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'#!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#Z!=!!?!!!7&amp;%JJ:W&gt;M:5*V&gt;(2P&lt;CZM&gt;G.M98.T!!!.5G6U&gt;8*O)%*V&gt;(2P&lt;A"+1(!!(A!!,"*4&gt;WFQ:3"49X*F:7YO&lt;(:M;7)85X&gt;J='5A5W.S:76O)&amp;6*,GRW9WRB=X-!!".4&gt;WFQ:3"49X*F:7YA65EA&lt;X6U!":!5!!$!!!!!1!##'6S=G^S)'FO!!"+1(!!(A!!,"*4&gt;WFQ:3"49X*F:7YO&lt;(:M;7)85X&gt;J='5A5W.S:76O)&amp;6*,GRW9WRB=X-!!"*4&gt;WFQ:3"49X*F:7YA65EA;7Y!!'%!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!"!!)!Q!!?!!!#1!!!!!!!!!*!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!1!!!.!!!!$!!!!!!!!!!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342972432</Property>
			</Item>
		</Item>
		<Item Name="Return to Origin Ref" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">Return to Origin Ref</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Return to Origin Ref</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="Get Return to Origin Reference.vi" Type="VI" URL="../Get Return to Origin Reference.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;]!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#B!=!!)!!!!#!!!'F*F&gt;(6S&lt;C"U&lt;S"0=GFH;7YA5G6G:8*F&lt;G.F!!"+1(!!(A!!,"*4&gt;WFQ:3"49X*F:7YO&lt;(:M;7)85X&gt;J='5A5W.S:76O)&amp;6*,GRW9WRB=X-!!".4&gt;WFQ:3"49X*F:7YA65EA&lt;X6U!":!5!!$!!!!!1!##'6S=G^S)'FO!!"+1(!!(A!!,"*4&gt;WFQ:3"49X*F:7YO&lt;(:M;7)85X&gt;J='5A5W.S:76O)&amp;6*,GRW9WRB=X-!!"*4&gt;WFQ:3"49X*F:7YA65EA;7Y!!'%!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!"!!)!Q!!?!!!$1A!!!!!!!!*!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!1!!!.!!!!$!!!!!!!!!!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342972432</Property>
			</Item>
		</Item>
		<Item Name="SwipeMenu Ref" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">SwipeMenu Ref</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">SwipeMenu Ref</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="Get SwipeMenu QControl Reference.vi" Type="VI" URL="../Get SwipeMenu QControl Reference.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'/!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$J!=!!?!!!4%6.X;8"F476O&gt;3ZM&gt;G.M98.T!"R4&gt;WFQ:5VF&lt;H5A57.P&lt;H2S&lt;WQA5G6G:8*F&lt;G.F!!"+1(!!(A!!,"*4&gt;WFQ:3"49X*F:7YO&lt;(:M;7)85X&gt;J='5A5W.S:76O)&amp;6*,GRW9WRB=X-!!".4&gt;WFQ:3"49X*F:7YA65EA&lt;X6U!":!5!!$!!!!!1!##'6S=G^S)'FO!!"+1(!!(A!!,"*4&gt;WFQ:3"49X*F:7YO&lt;(:M;7)85X&gt;J='5A5W.S:76O)&amp;6*,GRW9WRB=X-!!"*4&gt;WFQ:3"49X*F:7YA65EA;7Y!!'%!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!"!!)!Q!!?!!!$1A!!!!!!!!*!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!1!!!.!!!!$!!!!!!!!!!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
			</Item>
		</Item>
	</Item>
	<Item Name="Change Code View.vi" Type="VI" URL="../Change Code View.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)B!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"*!-`````]*1W^E:3"7;76X!%J!=!!?!!!M%F.X;8"F)&amp;.D=G6F&lt;CZM&gt;GRJ9B&gt;4&gt;WFQ:3"49X*F:7YA65EO&lt;(:D&lt;'&amp;T=Q!!%V.X;8"F)&amp;.D=G6F&lt;C"633"P&gt;81!*5!8!!9"-1%S!4-".!%V!49!%6"J9X2V=G5A5W6M:7.U;7^O!":!5!!$!!!!!1!##'6S=G^S)'FO!!"(1"=!!B*4&gt;WFQ:5VF&lt;H5A55.P&lt;H2S&lt;WQ85X&gt;J='5A17ZJ&lt;7&amp;U;7^O)%RJ9H*B=HE!!".$&lt;W2F)&amp;:J:8=A5W6M:7.U;7^O!%^!&amp;Q!($%ZP)&amp;.F&lt;'6D&gt;'FP&lt;A2.&lt;X:F#5^W:8*T;'^P&gt;!2%=G&amp;H#&amp;.M&lt;X&gt;E&lt;X&gt;O#F.Q=GFO:W*B9WM*5W6M:7.U;7^O!!!(17.U;7^O=Q"+1(!!(A!!,"*4&gt;WFQ:3"49X*F:7YO&lt;(:M;7)85X&gt;J='5A5W.S:76O)&amp;6*,GRW9WRB=X-!!"*4&gt;WFQ:3"49X*F:7YA65EA;7Y!!'%!]!!-!!-!"!!&amp;!!9!"!!%!!=!"!!)!!E!#A!,!Q!!?!!!#1!!!!!!!!!*!!!!$1M!!!!!!!!!!!!!%!!!!!!!!!!+!!!!%!!!!"!!!!!1!!!.!!!!$!!!!!!!!!!!!!!"!!Q!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1115685392</Property>
	</Item>
	<Item Name="Close.vi" Type="VI" URL="../Close.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!$^!!!!#!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!":!5!!$!!!!!1!##'6S=G^S)'FO!!"+1(!!(A!!,"*4&gt;WFQ:3"49X*F:7YO&lt;(:M;7)85X&gt;J='5A5W.S:76O)&amp;6*,GRW9WRB=X-!!"*4&gt;WFQ:3"49X*F:7YA65EA;7Y!!&amp;1!]!!-!!-!"!!%!!1!"!!%!!1!"!!&amp;!!1!"!!'!Q!!?!!!$1A!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!#1!!!!!!%!"Q!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1115685392</Property>
	</Item>
	<Item Name="Determine Return to Origin Visibility.vi" Type="VI" URL="../Determine Return to Origin Visibility.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;D!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%J!=!!?!!!M%F.X;8"F)&amp;.D=G6F&lt;CZM&gt;GRJ9B&gt;4&gt;WFQ:3"49X*F:7YA65EO&lt;(:D&lt;'&amp;T=Q!!%V.X;8"F)&amp;.D=G6F&lt;C"633"P&gt;81!&amp;E"1!!-!!!!"!!)):8*S&lt;X)A;7Y!!!^!!Q!)5'^T;82J&lt;WY!!%J!=!!?!!!M%F.X;8"F)&amp;.D=G6F&lt;CZM&gt;GRJ9B&gt;4&gt;WFQ:3"49X*F:7YA65EO&lt;(:D&lt;'&amp;T=Q!!%F.X;8"F)&amp;.D=G6F&lt;C"633"J&lt;A!!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!(!!A$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!%!!!!"!!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
	</Item>
	<Item Name="Initialize.vi" Type="VI" URL="../Initialize.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'F!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%J!=!!?!!!M%F.X;8"F)&amp;.D=G6F&lt;CZM&gt;GRJ9B&gt;4&gt;WFQ:3"49X*F:7YA65EO&lt;(:D&lt;'&amp;T=Q!!%V.X;8"F)&amp;.D=G6F&lt;C"633"P&gt;81!&amp;E"1!!-!!!!"!!)):8*S&lt;X)A;7Y!!%B!=!!?!!!Q&amp;5&amp;D&gt;'^S)%:S97VF&gt;W^S;SZM&gt;GRJ9BB.:8.T97&gt;F)%6O=86F&gt;76S,GRW9WRB=X-!!!V4:7RG)%6O=86F&gt;76S!":!=!!)!!!!!A!!#6:*)&amp;*F:GZV&lt;1"+1(!!(A!!,"*4&gt;WFQ:3"49X*F:7YO&lt;(:M;7)85X&gt;J='5A5W.S:76O)&amp;6*,GRW9WRB=X-!!"*4&gt;WFQ:3"49X*F:7YA65EA;7Y!!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!=!#!!*!Q!!?!!!#1!!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!%A!!!"!!!!#3!!!!!!%!#A!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
	</Item>
	<Item Name="Menu Selection.vi" Type="VI" URL="../Menu Selection.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;K!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%J!=!!?!!!M%F.X;8"F)&amp;.D=G6F&lt;CZM&gt;GRJ9B&gt;4&gt;WFQ:3"49X*F:7YA65EO&lt;(:D&lt;'&amp;T=Q!!%V.X;8"F)&amp;.D=G6F&lt;C"633"P&gt;81!&amp;E"1!!-!!!!"!!)):8*S&lt;X)A;7Y!!":!=!!)!!!!"A!!#5VF&lt;H5A382F&lt;1"+1(!!(A!!,"*4&gt;WFQ:3"49X*F:7YO&lt;(:M;7)85X&gt;J='5A5W.S:76O)&amp;6*,GRW9WRB=X-!!"*4&gt;WFQ:3"49X*F:7YA65EA;7Y!!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"Q!)!Q!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!"!!!!!1!!!.!!!!$!!!!!!!!!!!!!!"!!E!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
	</Item>
	<Item Name="Register for Events.vi" Type="VI" URL="../Register for Events.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(@!!!!$A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!":!=!!)!!!!"A!!#5VF&lt;H5A382F&lt;1!;1(!!'1!"!!505X&gt;J='6.:7ZV)%6W:7ZU!!^!!Q!)5'^T;82J&lt;WY!!"Z!=!!:!!%!"R*197YA1W^N='RF&gt;'5A28:F&lt;H1!!#Z!=!!8!!!!!A!"!!!$[!!'!!%!!!0I!!A45X&gt;J='5A5W.S:76O)%6W:7ZU=Q"+1(!!(A!!,"*4&gt;WFQ:3"49X*F:7YO&lt;(:M;7)85X&gt;J='5A5W.S:76O)&amp;6*,GRW9WRB=X-!!".4&gt;WFQ:3"49X*F:7YA65EA&lt;X6U!":!5!!$!!!!!1!##'6S=G^S)'FO!!"+1(!!(A!!,"*4&gt;WFQ:3"49X*F:7YO&lt;(:M;7)85X&gt;J='5A5W.S:76O)&amp;6*,GRW9WRB=X-!!"*4&gt;WFQ:3"49X*F:7YA65EA;7Y!!'%!]!!-!!-!"!!*!!I!"!!%!!1!"!!,!!1!"!!-!A!!?!!!$1A!!!!!!!!*!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!1!!!.!!!!$!!!!!!!!!!!!!!"!!U!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1342710272</Property>
	</Item>
	<Item Name="Resize Menu Bar.vi" Type="VI" URL="../Resize Menu Bar.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;5!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%J!=!!?!!!M%F.X;8"F)&amp;.D=G6F&lt;CZM&gt;GRJ9B&gt;4&gt;WFQ:3"49X*F:7YA65EO&lt;(:D&lt;'&amp;T=Q!!%V.X;8"F)&amp;.D=G6F&lt;C"633"P&gt;81!&amp;E"1!!-!!!!"!!)):8*S&lt;X)A;7Y!!%J!=!!?!!!M%F.X;8"F)&amp;.D=G6F&lt;CZM&gt;GRJ9B&gt;4&gt;WFQ:3"49X*F:7YA65EO&lt;(:D&lt;'&amp;T=Q!!%F.X;8"F)&amp;.D=G6F&lt;C"633"J&lt;A!!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!"!!!!U!!!!-!!!!!!!!!!!!!!%!#!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
	</Item>
	<Item Name="Return to Origin.vi" Type="VI" URL="../Return to Origin.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;5!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%J!=!!?!!!M%F.X;8"F)&amp;.D=G6F&lt;CZM&gt;GRJ9B&gt;4&gt;WFQ:3"49X*F:7YA65EO&lt;(:D&lt;'&amp;T=Q!!%V.X;8"F)&amp;.D=G6F&lt;C"633"P&gt;81!&amp;E"1!!-!!!!"!!)):8*S&lt;X)A;7Y!!%J!=!!?!!!M%F.X;8"F)&amp;.D=G6F&lt;CZM&gt;GRJ9B&gt;4&gt;WFQ:3"49X*F:7YA65EO&lt;(:D&lt;'&amp;T=Q!!%F.X;8"F)&amp;.D=G6F&lt;C"633"J&lt;A!!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!"!!!!U!!!!-!!!!!!!!!!!!!!%!#!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1115685392</Property>
	</Item>
</LVClass>
